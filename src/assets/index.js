import RegisterBg from './image/register-bg.jpg';
import LoginBg from './image/login-bg.jpg';
import LogoLinux from './image/linux-logo.png';

// icon
import ICFacebook from './icon/facebook.svg';
import ICTwitter from './icon/twitter.svg';
import ICYoutube from './icon/youtube.svg';
import ICInstagram from './icon/instagram.svg';
import ICLinkedin from './icon/linkedin.svg';

export {
  RegisterBg,
  LoginBg,
  LogoLinux,
  ICFacebook,
  ICTwitter,
  ICLinkedin,
  ICYoutube,
  ICInstagram,
};
